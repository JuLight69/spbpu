import os
import re
from itertools import cycle

f_dict=dict.fromkeys(['a','b','c','d','e','f','g','h','i','j','k','l',
                      'm','n','o','p','q','r','s','t','u','v','w','x','y','z',' '],0)
word_dict={}
def dict_word(dict,list):
    for i in range(len(list)):
        word=list[i]
        if(word in dict):
            dict[word]=dict[word]+1
        else:
            dict[word]=1
    return dict
#with open('SMSSpamCollection.txt','r',encoding='utf-8') as sms:
with open('C:/Users/User/Desktop/Panda/AB_NYC_2019.csv', 'r', encoding='utf-8') as sms:
    spam=0
    ham=0
    for line in sms:
        key,text=line.split('\t',maxsplit=1)
        if key=='spam':
            file_name='C:/Users/User/PycharmProjects/MagistrPy/spam/fs{}.txt'.format(spam)
            with open(file_name,'r+',encoding='utf-8') as fs:
                fs.write(text)
                for i in range(os.path.getsize(file_name)):
                    a = fs.read(1).lower()
                    if (a in f_dict):
                        f_dict[a] = f_dict[a] + 1
                fs.seek(0)
                word_list=re.findall(r'\w+',fs.read().lower())
                word_dict=dict_word(word_dict,word_list)
            spam=spam+1
        elif key=='ham':
            file_name='C:/Users/User/PycharmProjects/MagistrPy/ham/fh{}.txt'.format(ham)
            with open(file_name,'r+',encoding='utf-8') as fh:
                fh.write(text)
                for i in range(os.path.getsize(file_name)):
                    a = fh.read(1).lower()
                    if (a in f_dict):
                        f_dict[a] = f_dict[a] + 1
                fh.seek(0)
                word_list = re.findall(r'\w+', fh.read().lower())
                word_dict = dict_word(word_dict, word_list)
            ham=ham+1
        else:
            print('error')

#составление списка размеров файлов в папке , поиск максимального,минимального и среднего значений.

def Max_min_medi(path):
    i = 0
    files_size = []
    while(1):
        file_name = path.format(i)
        try:
            size=os.path.getsize(file_name)
        except(OSError):
            break
        files_size.append(size)
        i=i+1
    max_s=max(files_size)
    min_s=min(files_size)
    medi_s= sum(files_size)/len(files_size)

    return medi_s,max_s,min_s
#поиск максимального,минимального и среднего значений для совокупности файлов из папок spam /ham
max_all=max(Max_min_medi('C:/Users/User/PycharmProjects/MagistrPy/spam/fs{}.txt')[1],Max_min_medi('C:/Users/User/PycharmProjects/MagistrPy/ham/fh{}.txt')[1])
min_all=min(Max_min_medi('C:/Users/User/PycharmProjects/MagistrPy/spam/fs{}.txt')[2],Max_min_medi('C:/Users/User/PycharmProjects/MagistrPy/ham/fh{}.txt')[2])
medi_all=(Max_min_medi('C:/Users/User/PycharmProjects/MagistrPy/spam/fs{}.txt')[0]+
          Max_min_medi('C:/Users/User/PycharmProjects/MagistrPy/ham/fh{}.txt')[0])/2
#print(max_all,min_all,medi_all)

sort_word_dict={}
sort_dict = {}
def sort_value_dict(sort_dict,dict,f_name):
    list_d = list(dict.items())
    list_d.sort(key=lambda i: i[1],reverse=True)
    for i in list_d:
        sort_dict[i[0]]=i[1]
    name='{}.txt'.format(f_name)
    with open(name,'w',encoding='utf-8') as d:
        d.write(str(sort_dict))
    return sort_dict
sort_dict=sort_value_dict(sort_dict,f_dict,"sort_dict_litters")
sort_word_dict=sort_value_dict(sort_word_dict,word_dict,'sort_word_dict')
