import math as mh
import datetime
import itertools

def arithmetic (a,b,operation):
    if(operation is "+"):
        result=a+b
    elif (operation == '-'):
        result = a - b
    elif (operation == '*'):
        result = a * b
    elif (operation == '/'):
        result = a / b
    else:
        result='unknown operation'
    return result
#print(arithmetic(2,4,'p'))

def is_year_leap(years):
    if not (years % 4):
        if (years % 100):
            result=True
        elif not (years % 400):
            result = True
    else:
        result=False
    return result
#print(is_year_leap(1992))

def square(a):
    info=(a*4,a*a,a*mh.sqrt(2))
    return info
#print(square(5))

def season(n):
    if (n in range(3,6)):
        seas ='spring'
    elif (n in range(6,9)):
        seas ='summer'
    elif (n in range(9,12)):
        seas='autumn'
    elif (n in (12,1,2)):
        seas ='winter'
    else:
        seas ='error'
    return seas
#print (season(12))

def bank(a,years):
    money=a
    for i in range(years-1):
        money = money + money*0.1
    return money
#print(bank(100,1))

def is_prime(a):
    k=0
    if (a in (0,1)):
        a='false'
        return a
    for i in range(2,int(mh.sqrt(a)+1)):
        if(a%i==0):
            k+=1
    if k==0:
        a='true'
    else :
        a='false'
    return a
#print(is_prime(119))

def date_cheat(day, month, year):
    try:
        datetime.date(year, month, day)
    except ValueError:
        return False
    else:
        return True
#print(date_cheat(28,2,2019))

def XOR_cipher(str, key):
    result = []
    key = itertools.cycle(key)
    for i, k in zip(str, key):
        result.append(chr(ord(i) ^ ord(k)))
    return result
XOR_uncipher = XOR_cipher
kikis=XOR_cipher('kukis','s')
print(XOR_uncipher(kikis,'s'))



